<?php

declare(strict_types=1);

/**
 * @file
 * Theme settings form for RMHNF theme.
 */

use Drupal\Core\Form\FormState;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function rmhnf_form_system_theme_settings_alter(array &$form, FormState $form_state): void {



}
